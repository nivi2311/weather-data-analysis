# Imports Database class from the project to provide basic functionality for database access
from database import Database
# Imports ObjectId to convert to the correct format before querying in the db
from bson.objectid import ObjectId
import datetime

# User document contains username (String), email (String), and role (String) fields
class UserModel:
    USER_COLLECTION = 'users'

    def __init__(self, username):
        self._db = Database()
        self._latest_error = ''
        self.username = username
        self.user_document = self.__find({'username': self.username})

    
    # Latest error is used to store the error string in case an issue. It's reset at the beginning of a new function call
    @property
    def latest_error(self):
        return self._latest_error
    
    # Since username should be unique in users collection, this provides a way to fetch the user document based on the username
    def find_by_username(self, username):
        if  self.isAdmin():
            key = {'username': username}
            return self.__find(key)
        else:
            self._latest_error = "Requires Admin role to perform read action in User models"
            return -1
    
    # Finds a document based on the unique auto-generated MongoDB object id 
    def find_by_object_id(self, obj_id):
        if  self.isAdmin():
            key = {'_id': ObjectId(obj_id)}
            return self.__find(key)
        else:
            self._latest_error = "Requires Admin role to perform read action in User collections"
            return -1
    
    # Check if the user has admin role
    def isAdmin(self):
        if self.user_document != -1:
            if self.user_document['role'] == "admin":
                return True
            else:
                return False
        else:
            return False
    
    # Get device access details for a user
    def getDeviceAccess(self, device_id):
        if self.isAdmin():
            return "rw"
        else:
            checkDeviseAccess = res = next((item for item in self.user_document['device_access'] if item['device_id'] == device_id), None)
            if checkDeviseAccess != None:
                return checkDeviseAccess['access']
            else:
                return -1
    
    # Private function (starting with __) to be used as the base for all find functions
    def __find(self, key):
        user_document = self._db.get_single_data(UserModel.USER_COLLECTION, key)
        return user_document     


    # This first checks if a user already exists with that username. If it does, it populates latest_error and returns -1
    # If a user doesn't already exist, it'll insert a new document and return the same to the caller
    def insert(self, username, email, role, device_access):
        if  self.isAdmin():
            self._latest_error = ''
            user_document = self.find_by_username(username)
            if (user_document):
                self._latest_error = f'Username {username} already exists'
                return -1
            
            user_data = {'username': username, 'email': email, 'role': role, 'device_access': device_access}
            user_obj_id = self._db.insert_single_data(UserModel.USER_COLLECTION, user_data)
            return self.find_by_object_id(user_obj_id)
        else:
            self._latest_error = "Requires Admin role to perform write action in user collection"
            return -1


# Device document contains device_id (String), desc (String), type (String - temperature/humidity) and manufacturer (String) fields
class DeviceModel:
    DEVICE_COLLECTION = 'devices'

    def __init__(self, username):
        self._db = Database()
        self._latest_error = ''
        self.username = username
        self.user_access = UserModel(self.username)
    
    # Latest error is used to store the error string in case an issue. It's reset at the beginning of a new function call
    @property
    def latest_error(self):
        return self._latest_error
    
    # Since device id should be unique in devices collection, this provides a way to fetch the device document based on the device id
    def find_by_device_id(self, device_id):
        access = self.user_access.getDeviceAccess(device_id)
        if access in ['r', 'rw']:
            key = {'device_id': device_id}
            return self.__find(key)
        else:
            self._latest_error = "Insufficient permission to perform read action to the Device collection"
            return -1
    
    # Finds a document based on the unique auto-generated MongoDB object id 
    def find_by_object_id(self, obj_id):
        key = {'_id': ObjectId(obj_id)}
        device_document = self.__find(key)
        access = self.user_access.getDeviceAccess(device_document['device_id'])
        if access in ['r', 'rw']:
            return device_document
        else:
            self._latest_error = "Insufficient permission to perform read action to the Device collection"
            return -1
    
    # Private function (starting with __) to be used as the base for all find functions
    def __find(self, key):
        device_document = self._db.get_single_data(DeviceModel.DEVICE_COLLECTION, key)
        return device_document
    
    # This first checks if a device already exists with that device id. If it does, it populates latest_error and returns -1
    # If a device doesn't already exist, it'll insert a new document and return the same to the caller
    def insert(self, device_id, desc, type, manufacturer):
        access = self.user_access.getDeviceAccess(device_id)
        if access == 'rw':
            self._latest_error = ''
            device_document = self.find_by_device_id(device_id)
            if (device_document):
                self._latest_error = f'Device id {device_id} already exists'
                return -1
            
            device_data = {'device_id': device_id, 'desc': desc, 'type': type, 'manufacturer': manufacturer}
            device_obj_id = self._db.insert_single_data(DeviceModel.DEVICE_COLLECTION, device_data)
            return self.find_by_object_id(device_obj_id)
        else:
            self._latest_error = "Insufficient permission to perform write action to the Device collection"
            return -1


# Weather data document contains device_id (String), value (Integer), and timestamp (Date) fields
class WeatherDataModel:
    WEATHER_DATA_COLLECTION = 'weather_data'

    def __init__(self, username):
        self._db = Database()
        self._latest_error = ''
        self.username = username       
        self.user_access = UserModel(self.username)
    
    # Latest error is used to store the error string in case an issue. It's reset at the beginning of a new function call
    @property
    def latest_error(self):
        return self._latest_error
    
    # Since device id and timestamp should be unique in weather_data collection, this provides a way to fetch the data document based on the device id and timestamp
    def find_by_device_id_and_timestamp(self, device_id, timestamp):
        access = self.user_access.getDeviceAccess(device_id)
        if access in ['r', 'rw']:
            key = {'device_id': device_id, 'timestamp': timestamp}
            return self.__find(key)
        else:
            self._latest_error = "Insufficient permission to perform read action to the Device collection"
            return -1
    
    # Finds a document based on the unique auto-generated MongoDB object id 
    def find_by_object_id(self, obj_id):
        key = {'_id': ObjectId(obj_id)}
        wdata_document = self.__find(key)
        access = self.user_access.getDeviceAccess(wdata_document['device_id'])
        if access in ['r', 'rw']:
            return wdata_document
        else:
            self._latest_error = "Insufficient permission to perform read action to the Device collection"
            return -1

    # Private function (starting with __) to be used as the base for all find functions
    def __find(self, key):
        wdata_document = self._db.get_single_data(WeatherDataModel.WEATHER_DATA_COLLECTION, key)
        return wdata_document
    
    # This first checks if a data item already exists at a particular timestamp for a device id. If it does, it populates latest_error and returns -1.
    # If it doesn't already exist, it'll insert a new document and return the same to the caller
    def insert(self, device_id, value, timestamp):
        access = self.user_access.getDeviceAccess(device_id)
        if access == 'rw':
            self._latest_error = ''
            wdata_document = self.find_by_device_id_and_timestamp(device_id, timestamp)
            if (wdata_document):
                self._latest_error = f'Data for timestamp {timestamp} for device id {device_id} already exists'
                return -1
            
            weather_data = {'device_id': device_id, 'value': value, 'timestamp': timestamp}
            wdata_obj_id = self._db.insert_single_data(WeatherDataModel.WEATHER_DATA_COLLECTION, weather_data)
            return self.find_by_object_id(wdata_obj_id)
        else:
            self._latest_error = "Insufficient permission to perform write action to the Device collection"
            return -1


# Weather data document contains device_id (String), value (Integer), and timestamp (Date) fields
class DailyReportModel:
    DAILY_REPORTS_COLLECTION = 'daily_reports'
    WEATHER_DATA_COLLECTION = 'weather_data'

    def __init__(self):
        self._db = Database()
        self._latest_error = ''     
    
    # Latest error is used to store the error string in case an issue. It's reset at the beginning of a new function call
    @property
    def latest_error(self):
        return self._latest_error
    
    # Calculate and return Aggregates using data from weather data collection
    def get_aggregates_per_device_per_day(self):
        db_collection = self._db._db[DailyReportModel.WEATHER_DATA_COLLECTION]
        aggregate = db_collection.aggregate(
            [        
            {
                "$group" : 
                    {
                        "_id" :  { "month": { "$month": "$timestamp" },
                                   "day": { "$dayOfMonth": "$timestamp" },
                                   "year": { "$year": "$timestamp" },
                                   "device_id": "$device_id"
                        },
                        "Total data points" : {"$sum": 1},
                        "Average": {"$avg": "$value"},
                        "Maximum": {"$max": "$value"},
                        "Minimum": {"$min": "$value"}
                    }
            }]
        )
        return aggregate

    # Insert aggregates into daily reports collection
    def insert_aggregates_per_device_per_day(self):
        daily_reports_collection = self._db._db[DailyReportModel.DAILY_REPORTS_COLLECTION]
        daily_reports_collection.delete_many({})
        aggregates = self.get_aggregates_per_device_per_day()
        for aggregate in aggregates:
            device_id = aggregate['_id']['device_id']
            timestamp = datetime.datetime(year=aggregate['_id']['year'], month=aggregate['_id']['month'], day=aggregate['_id']['day'])
            aggregate_per_device_per_day = { "device_id": device_id, "timestamp": timestamp, "average": aggregate['Average'], "maximum": aggregate["Maximum"], "minimum": aggregate["Minimum"] }
            aggregate_data_obj_id = self._db.insert_single_data(DailyReportModel.DAILY_REPORTS_COLLECTION, aggregate_per_device_per_day)

    # Caculate and return Today's weather data aggregates
    def get_aggregates_per_device_today(self):
        db_collection = self._db._db[DailyReportModel.WEATHER_DATA_COLLECTION]
        today_beginning = datetime.datetime.combine(datetime.date.today(), datetime.time()) #datetime.datetime(2020, 12, 1, 0, 0, 0)
        today_end = datetime.datetime.combine(datetime.date.today() + datetime.timedelta(days=1), datetime.time()) #datetime.datetime(2020, 12, 2, 0, 0, 0
        aggregate = db_collection.aggregate(
            [ { "$match" : { "timestamp" : { "$gte": today_beginning, "$lt": today_end} } },        
            {
                "$group" : 
                    {
                        "_id" :  {
                                   "device_id": "$device_id"
                        },
                        "Total data points" : {"$sum": 1},
                        "Average": {"$avg": "$value"},
                        "Maximum": {"$max": "$value"},
                        "Minimum": {"$min": "$value"}
                    }
            }]
        )
        return aggregate
    
    #Insert today's aggregates to daily reprots collection
    def insert_aggregates_per_device_today(self):
        aggregates = self.get_aggregates_per_device_today()
        for aggregate in aggregates:
            device_id = aggregate['_id']['device_id']
            timestamp = datetime.datetime(date.today()) #datetime.datetime(2020, 12, 1, 0, 0, 0)
            aggregate_per_device_per_day = { "device_id": device_id, "timestamp": timestamp, "average": aggregate['Average'], "maximum": aggregate["Maximum"], "minimum": aggregate["Minimum"] }
            aggregate_data_obj_id = self._db.insert_single_data(DailyReportModel.DAILY_REPORTS_COLLECTION, aggregate_per_device_per_day)

    # Data retrieval function that takes a device id and a range of days , and returns average , minimum and maximum data values for each 
    # day, fetching data from the daily_reports collection.
    def find_aggregates_by_device_id_time_range(self, device_id, from_date, to_date):
        key = {"device_id": device_id, "timestamp": {"$lt": to_date, "$gte": from_date}}
        aggregate_data = self._db.get_data(DailyReportModel.DAILY_REPORTS_COLLECTION, key)
        return aggregate_data
        
