########################################
#First run the setup.py. 
#After successful DB setup, run main.py
########################################

from model import UserModel, DeviceModel, WeatherDataModel, DailyReportModel
from datetime import datetime

print("\n***************************************************")
print("Point 1 Implementation and test cases")
print("***************************************************")
# Shows how to initiate and search in the users collection based on a username
print("\n------------------------------------------------------")
print("Test Case 1 : Read User collection by default user \nExpected Result: Should Fail")
print("------------------------------------------------------")
user_coll = UserModel('user_1')
print("Executing Test case.....")
user_document = user_coll.find_by_username('user_1')
if (user_document == -1):
    print("Failed : " + user_coll.latest_error)
    print("Test Case Passed")
else:
    print("Passed : " + str(user_document))
    print("Test Case Failed")

# Shows a successful attempt on how to insert a user
print("\n-------------------------------------------------------------")
print("Test Case 2 : Insert into User collection by default user \nExpected Result: Should Fail")
print("-------------------------------------------------------------")
device_access = [{"device_id": "DT004", "access": "r"}, {"device_id": "DT005", "access": "rw"}]
print("Executing Test case.....")
user_document = user_coll.insert('test_3', 'test_3@example.com', 'default', device_access)
if (user_document == -1):
    print("Failed : " + user_coll.latest_error)
    print("Test Case Passed")
else:
    print("Passed : " + str(user_document))
    print("Test Case Failed")


# Shows how to initiate and search in the users collection based on a username
print("\n----------------------------------------------------")
print("Test Case 3 : Read User collection by admin user \nExpected Result: Should Pass")
print("----------------------------------------------------")
user_coll = UserModel('admin')
print("Executing Test case.....")
user_document = user_coll.find_by_username('user_1')
if (user_document == -1):
    print("Failed : " + user_coll.latest_error)
    print("Test Case Failed")
else:
    print("Passed : " + str(user_document))
    print("Test Case Passed")

# Shows a successful attempt on how to insert a user
print("\n----------------------------------------------------------")
print("Test Case 4 : Insert into User collection by admin user \nExpected Result: Should Pass")
print("----------------------------------------------------------")
device_access = [{"device_id": "DT004", "access": "r"}, {"device_id": "DT005", "access": "rw"}]
print("Executing Test case.....")
user_document = user_coll.insert('test_3', 'test_3@example.com', 'default', device_access)
if (user_document == -1):
    print("Failed : " + user_coll.latest_error)
    print("Test Case Failed")
else:
    print("Passed : " + str(user_document))
    print("Test Case Passed")

# Shows how to initiate and search in the devices collection based on a device id
print("\n-----------------------------------------------------")
print("Test Case 5 : Read Device collection by admin user \nExpected Result: Should Pass")
print("-----------------------------------------------------")
print("Executing Test case.....")
device_coll = DeviceModel('admin')
device_document = device_coll.find_by_device_id('DT002')
print(device_document)
device_object_id = device_document.get('_id')
if (device_document == -1):
    print("Failed : " + device_coll.latest_error)
    print("Test Case Failed")
else:
    print("Passed : " + str(device_document))
    print("Test Case Passed")

print("\n------------------------------------------------------------------------------")
print("Test Case 6 : Read Device collection by default user without device access \nExpected Result: Should Fail")
print("------------------------------------------------------------------------------")
print("Executing Test case.....")
device_coll = DeviceModel('test_3')
device_document = device_coll.find_by_device_id('DT002')
if (device_document == -1):
    print("Failed : " + device_coll.latest_error)
    print("Test Case Passed")
else:
    print("Passed : " + str(device_document))
    print("Test Case Failed")

print("\n--------------------------------------------------------------------------")
print("Test Case 7 : Read Device collection by default user with device access \nExpected Result: Should Pass")
print("--------------------------------------------------------------------------")
print("Executing Test case.....")
device_coll = DeviceModel('test_3')
device_document = device_coll.find_by_device_id('DT004')
if (device_document == -1):
    print("Failed : " + device_coll.latest_error)
    print("Test Case Failed")
else:
    print("Passed : " + str(device_document))
    print("Test Case Passed")

print("\n----------------------------------------------------------------------------------------------")
print("Test Case 8 : Read Device collection using object_id by default user without device access \nExpected Result: Should Fail")
print("----------------------------------------------------------------------------------------------")
print("Executing Test case.....")
device_coll = DeviceModel('test_3')
device_document = device_coll.find_by_object_id(device_object_id)
if (device_document == -1):
    print("Failed : " + device_coll.latest_error)
    print("Test Case Passed")
else:
    print("Passed : " + str(device_document))
    print("Test Case Failed")

print("\n------------------------------------------------------------------------------------------")
print("Test Case 9 : Read Device collection using object_id by default user with device access \nExpected Result: Should Pass")
print("------------------------------------------------------------------------------------------")
print("Executing Test case.....")
device_coll = DeviceModel('user_1')
device_document = device_coll.find_by_object_id(device_object_id)
if (device_document == -1):
    print("Failed : " + device_coll.latest_error)
    print("Test Case Failed")
else:
    print("Passed : " + str(device_document))
    print("Test Case Passed")


# Shows a successful attempt on how to insert a new device
print("\n------------------------------------------------------------------------------------------")
print("Test Case 10 : Insert into Device collection by admin user \nExpected Result: Should Pass")
print("------------------------------------------------------------------------------------------")
print("Executing Test case.....")
device_coll = DeviceModel('admin')
device_document = device_coll.insert('DT201', 'Temperature Sensor', 'Temperature', 'Acme')
if (device_document == -1):
    print("Failed : " + device_coll.latest_error)
    print("Test Case Failed")
else:
    print("Passed : " + str(device_document))
    print("Test Case Passed")


print("\n------------------------------------------------------------------------------------------")
print("Test Case 11 : Insert into Device collection by default user without device access \nExpected Result: Should Fail")
print("------------------------------------------------------------------------------------------")
print("Executing Test case.....")
device_coll = DeviceModel('user_2')
device_document = device_coll.insert('DT202', 'Temperature Sensor', 'Temperature', 'Acme')
if (device_document == -1):
    print("Failed : " + device_coll.latest_error)
    print("Test Case Passed")
else:
    print("Passed : " + str(device_document))
    print("Test Case Failed")

print("\n------------------------------------------------------------------------------------------")
print("Test Case 12 : Insert into Device collection by default user with device access \nExpected Result: Should Pass")
print("------------------------------------------------------------------------------------------")
print("Executing Test case.....")
device_coll = DeviceModel('user_1')
device_document = device_coll.insert('DT202', 'Temperature Sensor', 'Temperature', 'Acme')
if (device_document == -1):
    print("Failed : " + device_coll.latest_error)
    print("Test Case Failed")
else:
    print("Passed : " + str(device_document))
    print("Test Case Passed")

# Shows how to initiate and search in the weather_data collection based on a device_id and timestamp
print("\n-----------------------------------------------------")
print("Test Case 13 : Read Weather Data collection by admin user \nExpected Result: Should Pass")
print("-----------------------------------------------------")
print("Executing Test case.....")
wdata_coll = WeatherDataModel('admin')
wdata_document = wdata_coll.find_by_device_id_and_timestamp('DT002', datetime(2020, 12, 2, 13, 30, 0))
wdata_object_id = wdata_document.get('_id')
if (wdata_document == -1):
    print("Failed : " + wdata_coll.latest_error)
    print("Test Case Failed")
else:
    print("Passed : " + str(wdata_document))
    print("Test Case Passed")

print("\n------------------------------------------------------------------------------")
print("Test Case 14 : Read Weather Data collection by default user without device access \nExpected Result: Should Fail")
print("------------------------------------------------------------------------------")
print("Executing Test case.....")
wdata_coll = WeatherDataModel('test_3')
wdata_document = wdata_coll.find_by_device_id_and_timestamp('DT002', datetime(2020, 12, 2, 13, 30, 0))
if (wdata_document == -1):
    print("Failed : " + wdata_coll.latest_error)
    print("Test Case Passed")
else:
    print("Passed : " + str(wdata_document))
    print("Test Case Failed")

print("\n--------------------------------------------------------------------------")
print("Test Case 15 : Read Weather Data collection by default user with device access \nExpected Result: Should Pass")
print("--------------------------------------------------------------------------")
print("Executing Test case.....")
wdata_coll = WeatherDataModel('user_1')
wdata_document = wdata_coll.find_by_device_id_and_timestamp('DT002', datetime(2020, 12, 2, 13, 30, 0))
if (wdata_document == -1):
    print("Failed : " + wdata_coll.latest_error)
    print("Test Case Failed")
else:
    print("Passed : " + str(wdata_document))
    print("Test Case Passed")

print("\n----------------------------------------------------------------------------------------------")
print("Test Case 16 : Read Weather Data collection using object_id by default user without device access \nExpected Result: Should Fail")
print("----------------------------------------------------------------------------------------------")
print("Executing Test case.....")
wdata_coll = WeatherDataModel('test_3')
wdata_document = wdata_coll.find_by_object_id(wdata_object_id)
if (wdata_document == -1):
    print("Failed : " + wdata_coll.latest_error)
    print("Test Case Passed")
else:
    print("Passed : " + str(wdata_document))
    print("Test Case Failed")

print("\n------------------------------------------------------------------------------------------")
print("Test Case 17 : Read Weather Data collection using object_id by default user with device access \nExpected Result: Should Pass")
print("------------------------------------------------------------------------------------------")
print("Executing Test case.....")
wdata_coll = WeatherDataModel('user_1')
wdata_document = wdata_coll.find_by_object_id(wdata_object_id)
if (wdata_document == -1):
    print("Failed : " + wdata_coll.latest_error)
    print("Test Case Failed")
else:
    print("Passed : " + str(wdata_document))
    print("Test Case Passed")


# Shows a successful attempt on how to insert a new device
print("\n------------------------------------------------------------------------------------------")
print("Test Case 18 : Insert into Weather Data collection by admin user \nExpected Result: Should Fail with User exists")
print("------------------------------------------------------------------------------------------")
print("Executing Test case.....")
wdata_coll = WeatherDataModel('admin')
wdata_document = wdata_coll.insert('DT002', 12, datetime(2020, 12, 2, 13, 30, 0))
if (wdata_document == -1):
    print("Failed : " + wdata_coll.latest_error)
    if (wdata_coll.latest_error == "Data for timestamp 2020-12-02 13:30:00 for device id DT002 already exists"):
        print("Test Case Passed")
    else:
        print("Test Case Failed")
else:
    print("Passed : " + str(wdata_document))
    print("Test Case Passed")


print("\n------------------------------------------------------------------------------------------")
print("Test Case 19 : Insert into Weather Data collection by default user without device access \nExpected Result: Should Fail")
print("------------------------------------------------------------------------------------------")
print("Executing Test case.....")
wdata_coll = WeatherDataModel('test_3')
wdata_document = wdata_coll.insert('DT002', 12, datetime(2020, 12, 2, 13, 30, 0))
if (wdata_document == -1):
    print("Failed : " + wdata_coll.latest_error)
    print("Test Case Passed")
else:
    print("Passed : " + str(wdata_document))
    print("Test Case Failed")

print("\n------------------------------------------------------------------------------------------")
print("Test Case 20 : Insert into Weather Data collection by default user with device access \nExpected Result: Should Pass")
print("------------------------------------------------------------------------------------------")
print("Executing Test case.....")
wdata_coll = WeatherDataModel('user_1')
wdata_document = wdata_coll.insert('DT002', 12, datetime(2020, 12, 2, 13, 25, 0))
if (wdata_document == -1):
    print("Failed : " + wdata_coll.latest_error)
    print("Test Case Failed")
else:
    print("Passed : " + str(wdata_document))
    print("Test Case Passed")


print("\n***************************************************")
print("Point 2 Implementation and test cases")
print("***************************************************")

print("\nPOINT 2A --- Data Reports collection creation and aggregate data feeding")
daily_report = DailyReportModel()
#To populate today's aggregates in daily reports, call below function. Commented since we dont have today's data in weather data
#daily_report.insert_aggregates_per_device_today()
#To populate one time aggregates of all data present in weather data, call below function
daily_report.insert_aggregates_per_device_per_day()
print("\nPOINT 2A --- Done")

from_date = datetime(2020, 12, 1, 0, 0, 0)
to_date = datetime(2020, 12, 6, 0, 0, 0)

print("\nPOINT 2B --- Data Retrieval from daily reports collection")
print("\n------------------------------------------------------------------------------------------")
print("Aggregate Weather Data Values from " + str(from_date) + " to " + str(to_date))
print("------------------------------------------------------------------------------------------")


aggregate_data = daily_report.find_aggregates_by_device_id_time_range("DH003", from_date, to_date)

if aggregate_data:
    for data in aggregate_data:
        print("Date : " + str(data['timestamp']) + " Device ID : " + str(data['device_id']) + 
            " Average : " + str(data['average']) + " Maximum : " + str(data['maximum']) + " Minimum : " + str(data['minimum']))
else:
    print("Error: No data")